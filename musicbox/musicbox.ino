#include "pitches.h"
#include "button.h"
#include "buzzer.h"

#define PIN_BUTTON_MEL_ONE 3
#define PIN_BUTTON_OFF 5
#define PIN_BUZZER 6

Button buttonMelodyOne(PIN_BUTTON_MEL_ONE);
Button buttonOff(PIN_BUTTON_OFF);
Buzzer buzzer(PIN_BUZZER);

int notes[] = {NOTE_CS5, NOTE_C5, NOTE_CS5, NOTE_A4, NOTE_SILENCE, NOTE_A4, NOTE_GS4, NOTE_A4, NOTE_FS4, NOTE_SILENCE};
//double durations[] = {0.1, 8, 3, 8, 3, 0.1, 8, 3, 8, 3};
double durations[] = {0, 3, 8, 3, 8, 0.1, 3, 8, 3, 8, 0.1};
int melodyLength = 10;



// maybe somewhere in the future we will have one more button...
#define PIN_BUTTON_MEL_TWO 4
Button buttonMelodyTwo(PIN_BUTTON_MEL_TWO);

int notes2[] = {NOTE_C4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations2[] = {4, 1, 4, 1};
int melodyLength2 = 4;

void setup()
{
    buzzer.setMelody(notes, durations, melodyLength);
}

void loop()
{
    buzzer.playSound();

    if (buttonOff.wasPressed())
    {
        buzzer.turnSoundOff();
    }

    if (buttonMelodyOne.wasPressed())
    {
        buzzer.setMelody(notes, durations, melodyLength);
        buzzer.turnSoundOn();
    }
    if (buttonMelodyTwo.wasPressed())
    {
        buzzer.setMelody(notes2, durations2, melodyLength2);
        buzzer.turnSoundOn();
    }
}
